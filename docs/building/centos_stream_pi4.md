# Running AutoSD on Raspberry pi 4

The Automotive SIG publishes images built via [OSBuild](https://www.osbuild.org/)
based on the OSBuild manifest present in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

Here is a quick guide on how to get you started with them.

We recommand you download the `gadget` image to start with (see our
[Download images](../download_images.md) page for more information about the
different images built).


1. Download your rpi4 image from [https://autosd.sig.centos.org/AutoSD-9/nightly/](https://autosd.sig.centos.org/AutoSD-9/nightly/)
1. Unpack it.

    ```
    unxz auto-osbuild-rpi4-cs9-gadget-regular-aarch64-*.raw.xz
    ```

1. Flash it onto your SD card.

    !!! important

        Change the block device, shown as _`/dev/disk0`_ in the example, according to your system.


         ```
         sudo dd if=auto-osbuild-rpi4-cs9-gadget-regular-aarch64-*.raw of=/dev/disk0 status=progress bs=4M; sync
         ```


1. Insert the SD card into your Raspberry Pi 4.
1. Boot the Raspberry Pi 4.
1. Login as `root` or `guest` using the password: `password`.
1. Connect the Raspberry Pi 4 to the internet.

    !!! important "Wifi is not yet functional."

        If you have an ethernet cable, it will be the easiest solution.

        If you are not near to a router or cannot connect the pi to an ethernet cable, this is where
        the `gadget` image is interesting, see our page on [USB gadget](gadget.md) for more information
        on how to get it working.

1. Profit
